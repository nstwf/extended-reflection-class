<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\Reader;


final class SourceReader
{
    public function read(string $fileName, int $beforeLineNumber): string
    {
        $file = fopen($fileName, 'r');
        $line = 0;
        $source = '';

        while (!feof($file)) {
            if (++$line >= $beforeLineNumber) {
                break;
            }

            $source .= fgets($file);
        }

        fclose($file);

        return $source;
    }
}