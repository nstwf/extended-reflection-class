<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\UseStatement;


final class UseStatement
{
    public function __construct(
        private string $class,
        private ?string $alias = null
    ) {
    }

    public function getClass(): string
    {
        return $this->class;
    }

    public function getShortName(): string
    {
        return basename(str_replace('\\', '/', $this->class));
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }
}