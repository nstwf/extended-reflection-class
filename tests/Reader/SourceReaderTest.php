<?php


namespace Nstwf\ExtendedReflectionClass\Reader;


use PHPUnit\Framework\TestCase;


class SourceReaderTest extends TestCase
{
    public function testRead()
    {
        $sourceReader = new SourceReader();

        $expected = <<<HEREDOC
line 1

    line 3

HEREDOC;

        $this->assertEquals(
            $expected,
            $sourceReader->read(__DIR__ . '/file.txt', 4)
        );
    }
}