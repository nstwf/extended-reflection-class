<?php


namespace Nstwf\ExtendedReflectionClass\UseStatement;


use PHPUnit\Framework\TestCase;


class UseStatementTest extends TestCase
{
    public function testAlias()
    {
        $useStatement = new UseStatement(TestCase::class);

        $this->assertEquals(TestCase::class, $useStatement->getClass());
        $this->assertEquals(null, $useStatement->getAlias());
    }

    public function testNoAlias()
    {
        $useStatement = new UseStatement(TestCase::class, 'TestCaseAlias');

        $this->assertEquals(TestCase::class, $useStatement->getClass());
        $this->assertEquals('TestCaseAlias', $useStatement->getAlias());
    }

    public function testShortName()
    {
        $useStatement = new UseStatement(TestCase::class);

        $this->assertEquals('TestCase', $useStatement->getShortName());
    }
}