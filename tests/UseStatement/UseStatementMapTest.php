<?php


namespace Nstwf\ExtendedReflectionClass\UseStatement;


use PHPUnit\Framework\TestCase;


class UseStatementMapTest extends TestCase
{
    public function testHasByClassName()
    {
        $useStatementMap = UseStatementMap::new()
            ->add(new UseStatement(TestCase::class));

        $this->assertFalse($useStatementMap->hasUse('TestCaseAlias'));
        $this->assertTrue($useStatementMap->hasUse('TestCase'));
    }

    public function testHasByAlias()
    {
        $useStatementMap = UseStatementMap::new()
            ->add(new UseStatement(TestCase::class, 'TestCaseAlias'));

        $this->assertFalse($useStatementMap->hasUse('Test_Case'));
        $this->assertTrue($useStatementMap->hasUse('TestCaseAlias'));
    }

    public function testGetByClassName()
    {
        $useStatementMap = UseStatementMap::new()
            ->add(new UseStatement(TestCase::class));

        $this->assertEquals(new UseStatement(TestCase::class), $useStatementMap->get('TestCase'));
    }

    public function testGetByAlias()
    {
        $useStatementMap = UseStatementMap::new()
            ->add(new UseStatement(TestCase::class, 'TestCaseAlias'));

        $this->assertEquals(new UseStatement(TestCase::class, 'TestCaseAlias'), $useStatementMap->get('TestCaseAlias'));
    }

    public function testExceptionOnGetInvalidUse()
    {
        $useStatementMap = UseStatementMap::new()
            ->add(new UseStatement(TestCase::class, 'TestCaseAlias'));

        $this->expectException(\InvalidArgumentException::class);
        $useStatementMap->get('TestCaseAliasName');
    }
}