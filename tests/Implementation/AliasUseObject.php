<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\Implementation;


use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass as BarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass as BazClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass as FooBarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass as FooClassAlias;


final class AliasUseObject
{
    private BarClassAlias $bar;
    private FooClassAlias $foo;
    private BazClassAlias $baz;
    private FooBarClassAlias $fooBar;
}