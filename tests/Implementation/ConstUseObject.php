<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\Implementation;


use Nstwf\ExtendedReflectionClass\Implementation\Nested\BarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass;


use const PHP_INT_MAX;


final class ConstUseObject
{
    private BarClass $bar;
    private FooClass $foo;
    private BazClass $baz;
    private FooBarClass $fooBar;

    public function __invoke()
    {
        $a = PHP_INT_MAX;
    }
}