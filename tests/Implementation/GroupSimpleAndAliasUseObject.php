<?php


declare(strict_types=1);


namespace Nstwf\ExtendedReflectionClass\Implementation;


use Nstwf\ExtendedReflectionClass\Implementation\Nested\{BarClass as BarClassAlias};
use Nstwf\ExtendedReflectionClass\Implementation\Nested\BazClass;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooBarClass as FooBarClassAlias;
use Nstwf\ExtendedReflectionClass\Implementation\Nested\FooClass;


final class GroupSimpleAndAliasUseObject
{
    private BarClassAlias $bar;
    private FooClass $foo;
    private BazClass $baz;
    private FooBarClassAlias $fooBar;
}